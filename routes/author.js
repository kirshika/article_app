var express = require('express');
var router = express.Router();
var models = require('../models/index');

//to read data from author table
router.get('/', function (req, res, next) {
    models.author.findAll({})
        .then(authors => res.json({
            error: false,
            data: authors
        }))
        .catch(error => res.json({
            error: true,
            data: [],
            error: error
        }));

});

//to insert data to author table
router.post('/insert/', function (req, res, next) {
    const{name}=req.body;
    models.author.create({
        name: name
    }).then(authors => res.status(200).json({
        error: false,
        data: authors,
        message: 'New author is added successfully.'
    }))
        .catch(error => res.json({
            error: true,
            data: [],
            error: error
        }));
});

//to edit existing data in the author table
router.put('/update/:id', function (req, res, next) {
    const author_id = req.params.id;
    models.author.findById(author_id).then(
        authors => {
            if (authors) {
                res.status(200).json({
                    message: 'updated successfully'
                });
                return models.author.update({
                    name: req.body.name
                }, {
                    where: {
                        id: author_id
                    }
                });

            } else {
                res.status(200).json({
                    message: 'This id does not exist.'
                })
            }
        }
    ).catch(error => res.json({
        error: true,
        error: error
    }));

});

//to delete existing data from author table
router.delete('/delete/:id', function (req, res, next) {
    const author_id = req.params.id;
    models.author.findById(author_id).then(
        authors => {
            if (authors) {
                res.status(200).json({
                    message: 'an author was removed'
                });
                return models.author.destroy(
                    {
                        where: {
                            id: author_id
                        }
                    })

            } else {
                res.status(200).json({
                    message: 'This id does not exist.'
                })
            }
        }
    ).catch(error => res.json({
        error: true,
        error: error
    }));
});

module.exports = router;