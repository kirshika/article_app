'use strict';
module.exports = (sequelize, DataTypes) => {
  var author = sequelize.define('author', {
    name: {
        type:DataTypes.CHAR,
        allowNull:false,
        length:255,
        validate: {
            notEmpty: {
                msg: "You have to enter name of the author"
            }
        }
    }
  },{
      timestamps:true
  });
  author.associate = function(models) {
    // associations can be defined here
  };
  return author;
};