var express = require('express');
var router = express.Router();
var models = require('../models/index');

//read all data from article--------------------------------------------------------------------------------------------
router.get('/', function (req, res, next) {

    models.article.findAll({
        include: {
            model: models.author
        }
    }).then(
        articles => {
            if (articles) {

                var article0 = articles.map((article) => {
                    return {
                        id: article.id,
                        title: article.title,
                        author: article.author.name,
                        summary: article.content,
                        url: '/article/' + articles.id,
                        createdAt: article.createdAt
                    }
                });

                res.status(200).json({
                    message: 'result is found.',
                    statusCode: 200,
                    data: article0
                });

            } else {
                res.status(404).json({
                    statusCode:404,
                    message: 'Not found'
                })
            }
        }
    ).catch((error) => {
        console.log(error);
        res.json({
            statusCode:'400',
            error: true,
            error: 'Bad request'
        })
    });

});


//read one by id--------------------------------------------------------------------------------------------------------
router.get('/:id', function (req, res, next) {

    const article_id = req.params.id;
    models.article.findById(article_id, {
        include: {
            model: models.author
        }
    }).then(
        articles => {
            if (articles) {
                res.status(200).json({
                    message: 'result is found.',
                    statusCode: 200,
                    data: {
                        id: articles.id,
                        title: articles.title,
                        author: articles.author.name,
                        content: articles.content,
                        url: '/article/' + articles.id,
                        createdAt: articles.createdAt

                    }
                });

            } else {
                res.status(404).json({
                    statusCode:404,
                    message: 'Not found'
                })
            }
        }
    ).catch((error) => {
        console.log(error);
        res.json({
            statusCode:'400',
            error: true,
            error: 'Bad request'
        })
    });

});

//to insert data to article table -------------------------------------------------------------------------------------
router.post('/insert/', function (req, res, next) {
    const {author_id, title, content} = req.body;
    //console.log(author_id);
    models.author.findById(author_id).then(
        authors => {
            if (authors) {
                models.article.create({
                    author_id: author_id,
                    title: title,
                    content: content
                }).then(articles => res.status(200).json({
                    statusCode:'201',
                    error: false,
                    data: articles,
                    message: 'New article is added successfully.'
                }))
                    .catch(error => res.json({
                        error: true,
                        data: [],
                        error: 'enter all data. nothing can be empty.'
                    }));

            } else {
                res.status(404).json({
                    statusCode:404,
                    message: 'This author does not exist. '
                })
            }
        }
    ).catch(error => res.json({
        statusCode:'400',
        error: true,
        error: 'Bad request'
    }));


});


//to edit existing data in the article table ---------------------------------------------------------------------------
router.put('/update/:id', function (req, res, next) {
    const article_id = req.params.id;
    const {author_id, title, content} = req.body;
    models.article.findById(article_id).then(
        articles => {
            if (articles) {
                models.author.findById(author_id).then(
                    authors => {
                        if (authors) {
                            return models.article.update({
                                author_id: author_id,
                                title: title,
                                content: content
                            }, {
                                where: {
                                    id: article_id
                                }
                            })
                                .then(articles => res.status(200).json({
                                    statusCode:200,
                                    error: false,
                                    data: articles,
                                    message: 'Article is updated successfully.'
                                }))
                                .catch(error => res.json({
                                    error: true,
                                    data: [],
                                    error: error
                                }));

                        } else {
                            res.status(404).json({
                                statusCode:404,
                                message: 'This author does not exist. '
                            })
                        }
                    }
                )

            } else {
                res.status(200).json({
                    message: 'invalid id'
                })
            }
        }
    ).catch(error => res.json({
        statusCode:'400',
        error: true,
        error: 'Bad request'
    }));

});

//to delete existing data from article table----------------------------------------------------------------------------
router.delete('/delete/:id', function (req, res, next) {
    const article_id = req.params.id;
    models.article.findById(article_id).then(
        articles => {
            if (articles) {
                res.status(200).json({
                    message: 'an article was removed'
                });
                return models.article.destroy(
                    {
                        where: {
                            id: article_id
                        }
                    })

            } else {
                res.status(404).json({
                    statusCode:404,
                    message: 'Not found'
                })
            }
        }
    ).catch(error => res.json({
        statusCode:'400',
        error: true,
        error: 'Bad request'
    }));

});

module.exports = router;