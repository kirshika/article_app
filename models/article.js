'use strict';
module.exports = (sequelize, DataTypes) => {
    var article = sequelize.define('article', {
        author_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'author',
                key: 'id'
            },
            onUpdate: 'cascade',
            onDelete: 'cascade',
            validate: {
                notEmpty: {
                    msg: "Enter an author."
                }
            }
        },
        title: {
            type: DataTypes.CHAR,
            allowNull: false,
            validate: {
                len: {
                    args: [1, 255],
                    msg: "Title should be length of less than 255."
                },
                notEmpty: {
                    msg: "Title is required."
                }
            }
        },
        content: {
            type: DataTypes.TEXT,
            allowNull: false,
            validate: {
                notEmpty: {
                    msg: "content should be included."
                }
            }
        }
    }, {
        timestamps: true
    });
    article.associate = function (models) {
        // associations can be defined here
    };
    return article;
};